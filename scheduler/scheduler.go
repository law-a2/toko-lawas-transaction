package scheduler

import (
	"log"
	"time"
	"encoding/json"

	"gitlab.com/law-a2/toko-lawas-transaction/dto"
	amqp "github.com/rabbitmq/amqp091-go"
	"gitlab.com/law-a2/toko-lawas-transaction/repositories"
)


type Scheduler interface {
	ExpireTransactions()
}

type scheduler struct {
	transactionRepository repositories.TransactionRepository
}

func NewScheduler(p repositories.TransactionRepository) Scheduler {
	return &scheduler{
		transactionRepository: p,
	}
}

func (s scheduler) notify(username string, content string) {
	conn, err := amqp.Dial("amqp://guest:guest@35.223.24.101:5672/")
	failOnError(err, "Failed to connect to RabbitMQ")
	defer conn.Close()

	ch, err := conn.Channel()
	failOnError(err, "Failed to open a channel")
	defer ch.Close()

	q, err := ch.QueueDeclare(
		"notifications", // name
		false,           // durable
		false,           // delete when unused
		false,           // exclusive
		false,           // no-wait
		nil,             // arguments
	)
	failOnError(err, "Failed to declare a queue")

	product := &dto.NewNotificationRequest{
		Secret:   "pakabarGeng?moga2AmanLAWya",
		Username: username,
		Title:    "Notifikasi tentang produk kamu",
		Content:  content,
	}

	body, err := json.Marshal(product)
	err = ch.Publish(
		"",     // exchange
		q.Name, // routing key
		false,  // mandatory
		false,  // immediate
		amqp.Publishing{
			ContentType: "application/json",
			Body:        []byte(body),
		})
	failOnError(err, "Failed to publish a message")
	log.Printf(" [x] Sent %s\n", string(body))
}

func (s scheduler) ExpireTransactions() {
	
	allTransactions , err := s.transactionRepository.GetAll()

	if( err != nil ){
		log.Println(err)
	}
	for _, t:= range allTransactions{
		
		diff := time.Since(t.CreatedAt)
		if((diff >= (10 * time.Minute)) && t.Status == "PENDING"){
			t.Status = "CANCELLED"
			s.transactionRepository.Update(t.ID, t)
			s.notify(t.Username, "transaksi dibatalkan toko")
		}
	}
}

func failOnError(err error, msg string) {
	if err != nil {
		log.Panicf("%s: %s", msg, err)
	}
}