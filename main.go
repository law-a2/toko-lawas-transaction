package main

import (
	"log"
	"os"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"github.com/joho/godotenv"

	"gitlab.com/law-a2/toko-lawas-transaction/database"
	"gitlab.com/law-a2/toko-lawas-transaction/models"

	routers "gitlab.com/law-a2/toko-lawas-transaction/routers"
)

func main() {
	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
	}

	db, err := database.GetClient()
	db.AutoMigrate(&models.Transaction{})
	if err != nil {
		panic("Failed to connect database")
	}

	ginMode, exists := os.LookupEnv("GIN_MODE")
	if !exists {
		ginMode = "debug"
	}
	gin.SetMode(ginMode)

	router := gin.Default()
	config := cors.DefaultConfig()
	config.AllowAllOrigins = true
	config.AllowMethods = []string{"POST", "PUT", "PATCH", "DELETE"}
	config.AllowHeaders = []string{"*"}
	config.ExposeHeaders = []string{"*"}
	router.Use(cors.New(config))
	routers.SetupRouter(router)

	port, exists := os.LookupEnv("PORT")
	if !exists {
		port = "8080"
	}
	router.Run(":" + port)
}
