package controllers

import (
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"gitlab.com/law-a2/toko-lawas-transaction/dto"
	"gitlab.com/law-a2/toko-lawas-transaction/models"
	"gitlab.com/law-a2/toko-lawas-transaction/services"
)

type TransactionController interface {
	Setup(router *gin.RouterGroup)
	CreateTransaction(c *gin.Context)
	GetTransactionsByUsername(c *gin.Context)
	GetAllTransactions(c *gin.Context)
	DeleteTransaction(c *gin.Context)
	UpdateTransaction(c *gin.Context)
}

type transactionController struct {
	transactionService services.TransactionService
}

func NewTransactionController(p services.TransactionService) TransactionController {
	return &transactionController{
		transactionService: p,
	}
}

func (p transactionController) Setup(router *gin.RouterGroup) {
	transactionAPI := router.Group("/transaction")
	transactionAPI.POST("", p.CreateTransaction)
	transactionAPI.GET("/:username", p.GetTransactionsByUsername)
	transactionAPI.GET("", p.GetAllTransactions)
	transactionAPI.DELETE("/:id", p.DeleteTransaction)
	transactionAPI.PUT("/:id", p.UpdateTransaction)
}

func (p transactionController) CreateTransaction(c *gin.Context) {
	var transaction dto.TransactionRequest
	err := c.ShouldBindJSON(&transaction)

	if err != nil {
		c.JSON(http.StatusBadRequest, dto.BaseResponse{
			Message: "INVALID_TRANSACTION_REQUEST",
		})
		return
	}

	res, err := p.transactionService.CreateTransaction(transaction)
	if err != nil {
		c.JSON(http.StatusInternalServerError, dto.BaseResponse{
			Message: err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, dto.BaseResponse{
		Message: "success create transaction",
		Data:    convertToResponse(res),
	})
}

func (p transactionController) GetTransactionsByUsername(c *gin.Context) {
	username := c.Param("username")

	res, err := p.transactionService.GetTransactionsByUsername(username)
	if err != nil {
		c.JSON(http.StatusInternalServerError, dto.BaseResponse{
			Message: err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, dto.BaseResponse{
		Message: "success get transaction by username",
		Data:    convertToListOfResponses(res),
	})
}

func (p transactionController) GetAllTransactions(c *gin.Context) {
	res, err := p.transactionService.GetAllTransactions()
	if err != nil {
		c.JSON(http.StatusInternalServerError, dto.BaseResponse{
			Message: err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, dto.BaseResponse{
		Message: "success get all transaction",
		Data:    convertToListOfResponses(res),
	})
}

func (p transactionController) DeleteTransaction(c *gin.Context) {
	id := c.Param("id")

	err := p.transactionService.DeleteTransaction(id)
	if err != nil {
		c.JSON(http.StatusInternalServerError, dto.BaseResponse{
			Message: err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, dto.BaseResponse{
		Message: "success delete question",
	})
}

func convertToResponse(transaction models.Transaction) dto.TransactionResponse {
	transactionResponse := dto.TransactionResponse{
		Id:         transaction.ID,
		IdStock:    transaction.IdStock,
		Username:   transaction.Username,
		Harga:      transaction.Harga,
		Jumlah:     transaction.Jumlah,
		TotalHarga: transaction.TotalHarga,
		Status:     transaction.Status,
	}

	return transactionResponse
}

func convertToListOfResponses(transactions []models.Transaction) dto.TransactionList {
	var transactionList []dto.TransactionResponse
	for _, transaction := range transactions {
		transactionResponse := convertToResponse(transaction)
		transactionList = append(transactionList, transactionResponse)
	}

	return dto.TransactionList{Transactions: transactionList}
}

func (p transactionController) UpdateTransaction(c *gin.Context) {
	id := c.Param("id")
	uid, err := strconv.ParseUint(id, 10, 32)

	if err != nil {
		c.JSON(http.StatusBadRequest, dto.BaseResponse{
			Message: "INVALID_ID",
		})
		return
	}
	var transaction dto.TransactionRequest
	err = c.ShouldBindJSON(&transaction)

	if err != nil {
		c.JSON(http.StatusBadRequest, dto.BaseResponse{
			Message: "INVALID_TRANSACTION_REQUEST",
		})
		return
	}

	res, err := p.transactionService.UpdateTransaction(uint(uid), transaction)
	if err != nil {
		c.JSON(http.StatusInternalServerError, dto.BaseResponse{
			Message: err.Error(),
		})
		return
	}

	data := convertToResponse(res)
	data.Id = uint(uid)

	c.JSON(http.StatusOK, dto.BaseResponse{
		Message: "success update transaction",
		Data:    data,
	})
}
