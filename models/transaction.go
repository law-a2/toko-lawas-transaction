package models

import (
	"gorm.io/gorm"
)

type Transaction struct {
	gorm.Model
	IdStock    string `json:"idStock"`
	Username   string `json:"username"`
	Harga      uint   `json:"harga"`
	Jumlah     uint   `json:"jumlah"`
	TotalHarga uint   `json:"totalHarga"`
	Status     string `json:"status"`
}
