package services

import (
	"encoding/json"
	"log"

	amqp "github.com/rabbitmq/amqp091-go"
	"gitlab.com/law-a2/toko-lawas-transaction/dto"
	"gitlab.com/law-a2/toko-lawas-transaction/models"
	"gitlab.com/law-a2/toko-lawas-transaction/repositories"
)

type TransactionService interface {
	CreateTransaction(dto.TransactionRequest) (models.Transaction, error)
	GetTransactionsByUsername(username string) ([]models.Transaction, error)
	GetById(id uint) (models.Transaction, error)
	GetAllTransactions() ([]models.Transaction, error)
	DeleteTransaction(id string) error
	UpdateTransaction(id uint, transaction dto.TransactionRequest) (models.Transaction, error)
}

type transactionService struct {
	transactionRepository repositories.TransactionRepository
}

func NewTransactionService(p repositories.TransactionRepository) TransactionService {
	return &transactionService{
		transactionRepository: p,
	}
}

func (p transactionService) CreateTransaction(request dto.TransactionRequest) (models.Transaction, error) {
	transaction := models.Transaction{
		IdStock:    request.IdStock,
		Username:   request.Username,
		Harga:      request.Harga,
		Jumlah:     request.Jumlah,
		TotalHarga: (request.Jumlah * request.Harga),
		Status:     "PENDING",
	}
	p.notify(request.Username, "Pesanan diterima penjual")

	return p.transactionRepository.Create(transaction)
}

func (p transactionService) GetTransactionsByUsername(username string) ([]models.Transaction, error) {
	return p.transactionRepository.GetByUsername(username)
}

func (p transactionService) GetAllTransactions() ([]models.Transaction, error) {
	return p.transactionRepository.GetAll()
}

func (p transactionService) DeleteTransaction(id string) error {
	return p.transactionRepository.Delete(id)
}

func failOnError(err error, msg string) {
	if err != nil {
		log.Panicf("%s: %s", msg, err)
	}
}

func (p transactionService) notify(username string, content string) {
	conn, err := amqp.Dial("amqp://guest:guest@35.223.24.101:5672/")
	failOnError(err, "Failed to connect to RabbitMQ")
	defer conn.Close()

	ch, err := conn.Channel()
	failOnError(err, "Failed to open a channel")
	defer ch.Close()

	q, err := ch.QueueDeclare(
		"notifications", // name
		false,           // durable
		false,           // delete when unused
		false,           // exclusive
		false,           // no-wait
		nil,             // arguments
	)
	failOnError(err, "Failed to declare a queue")

	product := &dto.NewNotificationRequest{
		Secret:   "pakabarGeng?moga2AmanLAWya",
		Username: username,
		Title:    "Notifikasi tentang produk kamu",
		Content:  content,
	}

	body, err := json.Marshal(product)
	err = ch.Publish(
		"",     // exchange
		q.Name, // routing key
		false,  // mandatory
		false,  // immediate
		amqp.Publishing{
			ContentType: "application/json",
			Body:        []byte(body),
		})
	failOnError(err, "Failed to publish a message")
	log.Printf(" [x] Sent %s\n", string(body))
}

func (p transactionService) GetById(id uint) (models.Transaction, error) {
	return p.transactionRepository.GetById(id)
}

func (p transactionService) UpdateTransaction(id uint, request dto.TransactionRequest) (models.Transaction, error) {
	transaction, err := p.GetById(id)
	if err != nil {
		return models.Transaction{}, err
	}

	transactionModel := models.Transaction{
		IdStock:    transaction.IdStock,
		Username:   request.Username,
		Harga:      request.Harga,
		Jumlah:     request.Jumlah,
		TotalHarga: (request.Jumlah * request.Harga),
		Status:     request.Status,
	}

	if(request.Status == "CONFIRMED"){
		p.notify(request.Username, "Pesanan dikonfirmasi penjual")
	}

	return p.transactionRepository.Update(id, transactionModel)
}
