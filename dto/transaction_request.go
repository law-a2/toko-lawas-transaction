package dto

type TransactionRequest struct {
	IdStock  string `json:"idStock"`
	Username string `json:"username"`
	Harga    uint   `json:"harga"`
	Jumlah   uint   `json:"jumlah"`
	Status   string `json:"status"`
}
