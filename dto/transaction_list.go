package dto

type TransactionList struct {
	Transactions []TransactionResponse `json:"transactions"`
}
