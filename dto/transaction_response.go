package dto

type TransactionResponse struct {
	Id         uint   `json:"id"`
	IdStock    string `json:"idStock"`
	Username   string `json:"username"`
	Harga      uint   `json:"harga"`
	Jumlah     uint   `json:"jumlah"`
	TotalHarga uint   `json:"totalHarga"`
	Status     string `json:"status"`
}
