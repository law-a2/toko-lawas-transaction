package repositories

import (
	"gitlab.com/law-a2/toko-lawas-transaction/models"
	"gorm.io/gorm"
)

type TransactionRepository interface {
	Create(models.Transaction) (models.Transaction, error)
	GetByUsername(username string) ([]models.Transaction, error)
	GetById(id uint) (models.Transaction, error)
	GetAll() ([]models.Transaction, error)
	Delete(id string) error
	Update(id uint, transaction models.Transaction) (models.Transaction, error)
}

type transactionRepository struct {
	db *gorm.DB
}

func NewTransactionRepository(db *gorm.DB) TransactionRepository {
	return &transactionRepository{
		db: db,
	}
}

func (t transactionRepository) Create(transaction models.Transaction) (models.Transaction, error) {
	res := t.db.Create(&transaction)
	return transaction, res.Error
}

func (t transactionRepository) GetByUsername(username string) ([]models.Transaction, error) {
	var transactions []models.Transaction
	res := t.db.Where("username = ?", username).Find(&transactions)

	return transactions, res.Error
}

func (t transactionRepository) GetAll() ([]models.Transaction, error) {
	var transactions []models.Transaction
	res := t.db.Find(&transactions)

	return transactions, res.Error
}

func (t transactionRepository) Delete(id string) error {
	res := t.db.Where("id = ?", id).Delete(&models.Transaction{})
	return res.Error
}

func (t transactionRepository) GetById(id uint) (models.Transaction, error) {
	var transaction models.Transaction
	res := t.db.Where("id = ?", id).First(&transaction)

	return transaction, res.Error
}

func (t transactionRepository) Update(id uint, transaction models.Transaction) (models.Transaction, error) {
	res := t.db.Where("id = ?", id).Updates(&transaction)
	return transaction, res.Error
}
