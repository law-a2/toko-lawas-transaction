package router

import (
	"log"
	"time"

	"github.com/gin-gonic/gin"
	"gitlab.com/law-a2/toko-lawas-transaction/controllers"
	"gitlab.com/law-a2/toko-lawas-transaction/database"
	"gitlab.com/law-a2/toko-lawas-transaction/repositories"
	"gitlab.com/law-a2/toko-lawas-transaction/services"
	"gitlab.com/law-a2/toko-lawas-transaction/scheduler"
	"github.com/go-co-op/gocron"
)

func SetupRouter(router *gin.Engine) {

	// database
	db, err := database.GetClient()
	if err != nil {
		log.Println("ERROR | database client error")
		return
	}

	transactionRepository := repositories.NewTransactionRepository(db)
	transactionService := services.NewTransactionService(transactionRepository)
	transactionController := controllers.NewTransactionController(transactionService)
	scheduler := scheduler.NewScheduler(transactionRepository)
	
	s := gocron.NewScheduler(time.UTC)
	s.Every(1).Minute().Do(scheduler.ExpireTransactions)
	s.StartAsync()

	base := router.Group("/api/v1")
	transactionController.Setup(base)
}
